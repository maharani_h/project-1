<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    protected $table = "pertanyaan";
    protected $fillable = ['judul','content', 'thumbnail', 'kategori_id'];

    public function kategori()
    {
    return $this->belongsTo('App\Kategori');
    }
    public function jawaban()
    {
    return $this->hasMany('App\Jawaban');
    }
}
