<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class KategoriController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth')->except(['index','show','edit']);

    // // $this->middleware('log')->only('index');
    // // $this->middleware('subscribed')->except('store');
    }
    // menampilkan form page
    public function create(){
        return view('kategori.create');
    }
    // function push data dari form page terpush ke DB engine
    public function store(Request $request){

        $validatedData = $request->validate(
        [
        'nama' => 'required'
        ]);

        $kategori = new kategori;

        $kategori->nama = $request->nama;
        $kategori->save();
    
        Alert::success('Berhasil', 'Berhasil Tambah Kategori');
        return redirect('/kategori');
    }

    // mengambil data dari database
    public function index()
    {
        $kategori = kategori::all();
        return view('kategori.index', compact('kategori'));
    }

    // fungsi mengambil detail kategoriing based on $id
    public function show($id){
        $kategori = kategori::find($id);
        return view('kategori.show', compact('kategori'));
    }
    // fungsi mengedit detail kategoriing based on $id
    public function edit($id)
    {
        $kategori = kategori::find($id);
        return view('kategori.edit', compact('kategori'));
    }
    // fungsi mengupdate data yang di edit / Post data
    public function update (Request $request, $id){
        $validatedData = $request->validate(
        [
        'nama' => 'required'
        ]);

        $kategori = kategori::find($id);

        $kategori->nama = $request['nama'];

        $kategori->save();
        
        return redirect('/kategori');
    }
    // fungsi menghapus data
    public function destroy($id){
        $kategori = kategori::find($id);
        $kategori->delete();
        Alert::warning('Berhasil', 'Berhasil Tambah Kategori');
        return redirect('/kategori');
    }
}
