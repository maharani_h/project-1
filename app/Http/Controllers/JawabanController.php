<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jawaban;
use illuminate\Support\Facades\Auth;

class JawabanController extends Controller
{
    public function store(Request $request){
        $request->validate(
        [
        'isi' => 'required'
        ]);

        $jawaban = new Jawaban;

        $jawaban->isi = $request->isi;
        $jawaban->user_id = Auth::id();
        $jawaban->soal_id = $request->soal_id;

        $jawaban->save();

        return redirect()->back();
    }
}
