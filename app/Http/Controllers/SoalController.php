<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Soal;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class SoalController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
    $soal = soal::all();
    return view('soal.index', compact('soal'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
    $kategori = DB::table('kategori')->get();
    return view('soal.create', compact('kategori'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {

    $request->validate(
    [
    'judul' => 'required',
    'content' => 'required',
    'kategori_id' => 'required',
    'thumbnail' => 'required|image|mimes:jpeg,png,jpg|max:2048',
    ]);

    $thumbnailName = time().'.'.$request->thumbnail->extension();
    $request->thumbnail->move(public_path('image'), $thumbnailName);

    $soal = new soal;

    $soal->judul = $request->judul;
    $soal->content = $request->content;
    $soal->kategori_id = $request->kategori_id;
    $soal->thumbnail = $thumbnailName;

    $soal->save();
    Alert::success('Berhasil', 'Berhasil Tambah Kategori');
    return redirect('/soal');
    }

    /**
    * Display the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
    $soal = soal::findOrFail($id);
    return view('soal.show', compact('soal'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
    $kategori = DB::table('kategori')->get();
    $soal = soal::findOrFail($id);

    return view('soal.edit', compact('soal','kategori'));


    }

    /**
    * Update the specified resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
    $request->validate(
    [
    'judul' => 'required',
    'content' => 'required',
    'kategori_id' => 'required',
    'thumbnail' => 'image|mimes:jpeg,png,jpg|max:2048',
    ]);

    $soal = Soal::find($id);

    if($request->has('thumbnail') ){
    $thumbnailName = time().'.'.$request->thumbnail->extension();
    $request->thumbnail->move(public_path('image'), $thumbnailName);

    // $soal = new soal; //buat data baru

    $soal->judul = $request->judul;
    $soal->content = $request->content;
    $soal->kategori_id = $request->kategori_id;
    $soal->thumbnail = $thumbnailName;
    }else {
    $soal->judul = $request->judul;
    $soal->content = $request->content;
    $soal->kategori_id = $request->kategori_id;
    }
    $soal->update();
    return redirect('/soal');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
    $soal = soal::find($id);

    $path = "image/";
    File::delete($path . $soal->thumbnail);
    $soal->delete();

    return redirect('/soal');
    }
}
