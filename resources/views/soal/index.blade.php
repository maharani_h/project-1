@extends('layout.master')

@section('judul')
<h1>Daftar soal</h1>
@endsection

@section('content')


{{-- @auth     --}}
<a href="/soal/create" class="btn btn-primary mb-3">Tambah</a>
{{-- @endauth --}}
<div class="row">
    @forelse ($soal as $item)
    <div class="col-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('image/'. $item->thumbnail)}}" alt="">
            <div class="card-body">
                <h3 class="card-text">{{$item->judul}}</h3>
                <p class="card-text text-justify">{{($item->content)}}</p>
                <span class="badge badge-info">{{$item->kategori->nama}}</span>
            @auth
            <form action="/soal/{{$item->id}}" method="post">
                @csrf
                @method('DELETE')
                <a href="/soal/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/soal/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>                
                    <form action="/soal/{{$item->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm" >
                    </form>
            </form>
            @endauth
            </div>
        </div>
    </div>
		@guest
		<a href="/soal/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
		@endguest
@empty
		<h1>Data not found</h1>
@endforelse
</div>
@endsection