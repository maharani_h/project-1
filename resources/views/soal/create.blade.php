@extends('layout.master')

@section('judul')
Input Data soals
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/q83rhgh35dkkm741yvz2zh04hfrd8z7hkofx4aygcnxrprto/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
// In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>
@endpush

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush


@section('content')
<form action="/soal" method="POST" enctype="multipart/form-data">
    @csrf
<div class="form-group">
    <label >Nama Judul soal</label>
    <input type="text" name="judul" class="form-control" >
</div>
@error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label >Deskripsi Soal</label>
    <textarea name="content" class="form-control"></textarea>
</div>
@error('content')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label >Kategori Soal</label> <br>
    <select name="kategori_id" class="js-example-basic-single" style="width: 100%" id="">
        <option value="">--- Pilih kategori ---</option>    
        @foreach ($kategori as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
        @endforeach
    </select>
</div>
@error('kategori_id')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label >Thumbnail Soal</label>
    <input type="file" name="thumbnail" class="form-control" >
</div>
@error('thumbnail')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection