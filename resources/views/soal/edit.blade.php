@extends('layout.master')

@section('judul')
Halaman Edit  soal
@endsection

@section('content')
<h2>Edit soal {{$soal->judul}}</h2>
<form action="/soal/{{$soal->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
<div class="form-group">
    <label >Judul soal</label>
    <input type="text" value="{{$soal->judul}}" name="judul" class="form-control" value="{{$soal->judul}}" >
</div>
@error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label >content</label>
    <textarea name="content" class="form-control">{{$soal->content}}</textarea>
</div>
@error('content')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label >Kategori Soal</label>
    <select name="kategori_id" class="form-control" id="">
        <option value="">--- Pilih Kategori ---</option>    
        @foreach ($kategori as $item)
            @if ($item ->id === $soal->kategori_id)
            <option value="{{$item->id}}" selected>{{$item->nama}}</option>

            @else
            <option value="{{$item->id}}">{{$item->nama}}</option>

            @endif

        @endforeach
    </select>
</div>
@error('kategori_id')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label >thumbnail</label>
    <input type="file" name="thumbnail" class="form-control" >
</div>
@error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection