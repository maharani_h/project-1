@extends('layout.master')

@section('judul')
Detail Soal 
@endsection

@section('content')
<img src="{{asset('image/'. $soal->thumbnail)}}" alt="">
<h3 class="card-text">{{$soal->judul}}</h3>
<p class="card-text text-justify">{{Str::limit($soal->content, 500)}}</p>
{{-- <span class="badge badge-info">{{$soal->kategori->nama}}</span> --}}
{{-- <p class="card-text">Genre : {{$soal->genre}} </p> --}}

<h2>Jawaban</h2>

@foreach ($soal->jawaban as $item)
<div class="card">
            <div class="card-body">
                <small><b>{{$item->user->name}}</b> </small>
                <p class="card-text text-justify">{{($item->isi)}}</p>
            </div>
        </div>
@endforeach

<form action="/jawaban" method="POST" enctype="multipart/form-data" class="my-3">
    @csrf
<div class="form-group">
    <label >Isi jawaban Anda </label>
    <input type="hidden" name="soal_id" value="{{$soal->id}}" id="">
    <textarea name="isi" class="form-control" cols="30" rows="10"></textarea>
</div>
@error('soal_id')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="p-2">
<button type="submit" class="btn btn-primary">Submit</button>

<a href="/soal" class="btn btn-primary">Kembali</a>
</div>
</form>
@endsection