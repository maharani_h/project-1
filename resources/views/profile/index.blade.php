@extends('layout.master')

@section('judul')
Profil
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/n0je507rdpxaukbmzdmq2w1s4l7fgr54qbvh8bdzd6ukpalv/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
    selector: 'textarea', //fokus di textarea 
    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
});
</script>
@endpush


@section('content')
<h2>Edit Profil</h2>
<form action="/profil/{{$profile->id}}" method="POST">
    @csrf
    @method('PUT')
    <label>Nama :</label> <br> <br>
    <input type="text" value="{{$profile->user->name}}" class="form-control" disabled> <br> <br>
    

    <label>E-mail :</label> <br> <br>
    <input type="text" value="{{$profile->user->email}}" class="form-control" disabled> <br> <br>
    

    <label>Usia :</label> <br> <br>
    <input type="number" name="umur" value="{{$profile->umur}}" class="form-control"> <br> <br>
    @error('umur')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <label>Alamat :</label> <br> <br>
    <textarea name="alamat" class="form-control"cols="50" rows="5">{{$profile->alamat}}</textarea> <br>
    @error('alamat')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <Label>Biodata :</Label> <br> <br>
    <textarea name="bio" class="form-control" cols="50" rows="5">{{$profile->bio}}</textarea> <br>
    @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Edit</button>
</form>


@endsection