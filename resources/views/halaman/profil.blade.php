@extends('layout.master')

@section('judul')
Profil
@endsection

@section('content')
<h2>Edit Profil</h2>
    <form action="/home" method="post">
        @csrf
        
        <label>Nama :</label> <br> <br>
        <input type="text" name="name"> <br> <br>
        <label>E-mail :</label> <br> <br>
        <input type="text" name="email"> <br> <br>
        <label>Usia :</label> <br> <br>
        <input type="text" name="umur"> <br> <br>
        <label>Alamat :</label> <br> <br>
        <textarea name="alamat" cols="50" rows="5"></textarea> <br>
        <Label>Biodata</Label> <br> <br>
        <textarea name="bio" cols="50" rows="5"></textarea> <br>
        <input type="submit" value="Edit">
    </form>
@endsection