@extends('layout.master')

@section('judul')
ASK.ME
@endsection

@section('content')
<h1>Welcome to ASK.ME</h1>
    
    <h3>Menjawab setiap pertanyaanmu</h3> <br><br>

    <p>Log in <a href="/login">di sini</a></p>
    
    <p>Belum punya akun? Klik <a href="/register">DAFTAR</a></p>
@endsection
