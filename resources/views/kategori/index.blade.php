@extends('layout.master')

@section('judul')
<h1>List kategori Soal</h1>
@endsection

@section('content')

@auth    
<a href="/kategori/create" class="btn btn-primary mb-3">Tambah</a>
@endauth
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">kategori</th>
      <th scope="col">List Soal</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      @forelse ($kategori as $key => $item)
          <tr>
              <td>{{$key + 1}} </td>
              <td>{{$item->nama}} </td>
              <td>
                <ul>
                  @foreach ($item->soal as $value)
                  <li>{{$value->judul}}</li>
                  @endforeach  
                </ul>  
              </td>
              @auth
              
              <td>
                <form action="/kategori/{{$item->id}}" method="post">
                  <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>                
                  @csrf
                  @method('delete')
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
              @endauth
              @guest
                <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              @endguest
          </tr>
          {{-- 13:00 --}}
      @empty
          <h1>Data not found</h1>
      @endforelse
  </tbody>
</table>
@endsection