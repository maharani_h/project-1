@extends('layout.master')

@section('judul')
Edit list kategori
@endsection

@section('content')
<h2>Edit list data Kategori {{$kategori->id}}</h2>
<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('PUT')
<div class="form-group">
    <label >kategori</label>
    <input type="text" name="nama" class="form-control" value="{{$kategori->nama}}" >
</div>
@error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection