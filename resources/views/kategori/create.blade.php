@extends('layout.master')

@section('judul')
Halaman Create Kategori Pertanyaan
@endsection

@section('content')
<form action="/kategori" method="POST">
    @csrf
<div class="form-group">
    <label >Kategori Pertanyaan</label>
    <input type="text" name="nama" class="form-control" >
</div>

@error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
