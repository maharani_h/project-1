@extends('layout.master')

@section('judul')
Detail Kategori {{$kategori->nama}}
@endsection

@section('content')
<h1 class="bg-dark">{{$kategori->nama}}</h1>

<div class="row">
    @foreach ($kategori->soal as $item)
    <div class="col-4 p-2">
        <div class="card">
            <img class="card-img-top" src="{{asset('image/'. $item->thumbnail)}}" alt="">
            <div class="card-body">
                <h3>{{$item->judul}}</h3>
                <p class="card-text text-justify">{{($item->content)}}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

<a href="/kategori" class="btn btn-success ms-3">Back</a>
@endsection