<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('halaman.index');
});

//Route::get('/master', function(){
   // return view('layout.master');
//}); cuma buat testing tamplate

Route::get('/welcome', function(){
    return view('halaman.welcomepage');
});


Route::group(['middleware' => ['auth']], function(){

    //update Profile
    Route::resource('profil', 'ProfileController')->only([
        'index', 'update'
    ]);
    // CRUD kategori
    // Create
    Route::GET('/kategori/create', 'KategoriController@create'); //menampilkan form untuk membuat data pemain kategori baru
    Route::post('/kategori', 'KategoriController@store'); //menyimpan data baru ke tabel kategori
    // Read
    Route::GET('/kategori', 'KategoriController@index');//menampilkan list data para pemain kategori
    Route::GET('/kategori/{kategori_id}', 'KategoriController@show');//menampilkan detail data pemain kategori dengan id tertentu
    // Update
    Route::GET('/kategori/{kategori_id}/edit','KategoriController@edit'); //route untuk menuju form edit
    Route::put('/kategori/{kategori_id}','KategoriController@update'); //route untuk update data based on id
    // Delete
    Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy'); // route untuk menghapus data di DB



});
Route::resource('soal', 'SoalController');
Route::resource('jawaban', 'JawabanController')->only(['index','store']); //route jawaban


Auth::routes();
