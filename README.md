# Kelompok 15 Group 1

## Tema

Membuat Forum Tanya Jawab

## Anggota

Qoirul Arifin -> Qooir45
Maharani Hamimi -> @MaharaniHamimi
Akhmad Fauzan Farhan -> @fauzan_farhan

## Link Video 

- [Demo Aplikasi](https://drive.google.com/file/d/19RYS4xHhi9WeYk_QImoj3PAFnArF_qRe/view?usp=sharing)

## ERD

<img src="/public/image/erd.png" alt="ERD">